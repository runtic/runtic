
import axios from 'axios'
import { setAutenticacionToken } from './setAutenticacionToken'
import jwt_decode from 'jwt-decode'
import {SET_USUARIO_ACTUAL} from '../reducers/tipos'
import { CREAR_RUTINAS_ENDPOINT, LOGIN_ENDPOINT, REGISTRO_ENDPOINT } from './endpoints'


export const loginUsuario = (datosUsuario) => dispatch =>{
    
    return new Promise((resolve, reject) => {

        axios.post(LOGIN_ENDPOINT,datosUsuario,{
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json'}
        }).then((response) => {
            //console.log(response)
            
            //la constante authorization viene del api 
            const {authorization} = response.headers

            localStorage.setItem('jwtToken',authorization)

            setAutenticacionToken(authorization)

            const decodificado = jwt_decode(authorization)

            dispatch(setUsuarioActual({usuario:decodificado, conectado:true}))
            
            resolve(response)

        }).catch((err) =>{
            reject(err)
        })
    })
    
}

export const setUsuarioActual =({usuario,conectado})=>{
    return{
        type:SET_USUARIO_ACTUAL,
        payload: {usuario, conectado}
    }
}

export const cerrarSesion=()=> dispatch=> {

    localStorage.removeItem('jwtToken')

    setAutenticacionToken(false)

    dispatch(setUsuarioActual({
        usuario:{},
        conectado:false
    }))
    window.location.href="/"

    

}

export const registrarUsuario=(datosUsuario)=>dispatch=>{
    
    return new Promise((resolve, reject) => {

        axios.post(REGISTRO_ENDPOINT,datosUsuario,{
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json'}
        }).then((response) => {
        
            resolve(response)

        }).catch((err) =>{
            reject(err)
        })
    })
}

export const crearRutina=(datosUsuario)=>dispatch=>{
    
    return new Promise((resolve, reject) => {

        axios.post(CREAR_RUTINAS_ENDPOINT,datosUsuario,{
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json'}
        }).then((response) => {
        
            resolve(response)

        }).catch((err) =>{
            reject(err)
        })
    })
}
