const API_URL = 'https://localhost:8080'

export const LOGIN_ENDPOINT = API_URL+"/usuarios/login"
export const REGISTRO_ENDPOINT = API_URL+"/usuarios"
export const RUTINAS_CREADAS_ENDPOINT = API_URL+"/rutinas/ultimos"
export const EDITAR_RUTINAS_ENDPOINT = API_URL+"/editarRutina"
export const CREAR_RUTINAS_ENDPOINT = API_URL+"/dashboard/crearRutina"