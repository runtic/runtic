import React,{Redirect} from 'react'
import { useSelector } from 'react-redux'
import { Router } from 'react-router-dom'

const RutaPrivada = ({component:Componente, ...restoPro}) => {
    
    const conectado= useSelector(state => state.auth.conectado)

    return (
        <Router
            {...restoPro}
            render={
                (propiedades) => conectado===true ? <Componente {...propiedades}/> : <Redirect to="/login"/>
            }
        />
    )
}

export {RutaPrivada}
