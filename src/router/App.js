import '../styles/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { BrowserRouter,Switch,Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import {Home} from '../pages/Home'
import {EditarRutina} from '../pages/EditarRutina'
import {CrearRutina} from '../pages/CrearRutina'
import { Singin } from '../pages/Singin';
import {Dashboard} from '../pages/Dashboard'
import { Singup } from '../pages/Singup';
import { Provider } from 'react-redux';
import { store } from '../store';
import {comprobarToken} from '../helpers/comprobarToken'
import {RutaPrivada} from './RutaPrivada'

comprobarToken()

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Container>
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/login" component={Singin}></Route>
            <Route exact path="/usuarios" component={Singup}></Route>
            <Route exact path="/dashboard" component={Dashboard}></Route>
            <RutaPrivada exact path="/editarRutina/:id" component={EditarRutina}></RutaPrivada>
            <Route exact path="/dashboard/crearRutina" component={CrearRutina}></Route>
          </Switch>
        </Container>
        
      </BrowserRouter>
    </Provider>
  )
}

export default App;
