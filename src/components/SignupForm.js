
import React, { useEffect, useState } from 'react'
import { Alert, Button, Form } from 'react-bootstrap'
import logo from '../img/Logo.png'
import AOS from 'aos';
import 'aos/dist/aos.css';
import { Link } from 'react-router-dom';

const SignupForm = ({errores, enviarCallback}) => {

    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")
    const [correo, setCorreo] = useState("")

    useEffect(() => {
        AOS.init({duration:2000});
    },[])

    const enviarFormulario=(e)=>{
        e.preventDefault();
        enviarCallback({userName,correo,password})
    }

    return (

        <div className="container" data-aos="zoom-out">
            <div className="row">
                <div className="col-md-5 col-lg-4 mx-auto">
                    <div className="myform form shadow  mb-5 bg-body rounded-3 ">
                        <div className="col-md-12 d-flex justify-content-center">
                            <Link className="navbar-brand" to={"/"}>
                                <img src={logo} width="150" height="100" alt=""/>
                            </Link>
                        </div>
                        <Form onSubmit={enviarFormulario}>
                        <Form.Group className="mb-3 text-secondary" controlId="userName">
                                <Form.Label>Usuario</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    value={userName}
                                    onChange={e=>setUserName(e.target.value)}
                                    isInvalid={errores.userName}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errores.userName}
                                </Form.Control.Feedback>
                                
                            </Form.Group>

                            <Form.Group className="mb-3 text-secondary" controlId="correo">
                                <Form.Label>Correo</Form.Label>
                                <Form.Control 
                                    type="test"
                                    value={correo}
                                    onChange={e=>setCorreo(e.target.value)}
                                    isInvalid={errores.correo}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errores.correo}
                                </Form.Control.Feedback>
                                
                            </Form.Group>
                        
                            <Form.Group className="mb-3 text-secondary" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password"
                                    value={password}
                                    onChange={e=>setPassword(e.target.value)}
                                    isInvalid={errores.password}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errores.password}
                                </Form.Control.Feedback>
                            </Form.Group>

                            {errores.registro && <Alert variant="danger">{errores.registro}</Alert>}
                            
                            <Button className="col-md-12 text-center mt-3" variant="primary" type="submit">
                                Registarme
                            </Button>
                        </Form>
                        <div className=" fontsizeForm mt-3">
                            <p className="text-center text-secondary">¿Ya te registraste? <Link to={'./login'} id="signup">Inicia sesion</Link></p>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    )
}
export {SignupForm}
