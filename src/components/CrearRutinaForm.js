import React, { useEffect, useState } from 'react'
import { Button, Form} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from '../img/Logo.png'
import AOS from 'aos';
import 'aos/dist/aos.css';

const CrearRutinaForm = ({errores, enviarCallback}) => {

    const [fecha, setFecha] = useState("")
    const [tipo, setTipo] = useState("")
    const [categoria, setCategoria] = useState("")
    const [detalle, setDetalle] = useState("")
    const [prioridad, setPrioridad] = useState("")
    

    useEffect(() => {
        AOS.init({duration:2000});
    },[])

    const enviarFormulario=(e)=>{
        e.preventDefault();
        enviarCallback({fecha,tipo,categoria,detalle,prioridad})
       
    }

    return (
        <div className="container mt-5" data-aos="zoom-out">
            <div className="row">
                <div className="col-md-5 col-lg-4 mx-auto">
                    <div className="myform form shadow  mb-5 bg-body rounded-3 ">
                        <div className="col-md-12 d-flex justify-content-center">
                            <Link className="navbar-brand" to={"/dashboard"}>
                                <img src={logo} width="150" height="100" alt=""/>
                            </Link>
                        </div>
                        <Form onSubmit={enviarFormulario}>

                            <Form.Group 
                                className="mt-3 mb-3" 
                                value={fecha}
                                onChange={e=>setFecha(e.target.value)}
                                isInvalid={errores.fecha}
                            >
                            <Form.Label>Fecha</Form.Label>
                            <Form.Control type="date"/>
                            <Form.Control.Feedback type="invalid">
                                    {errores.fecha}
                            </Form.Control.Feedback>

                            </Form.Group>
                            

                            <Form.Group 
                            
                                value={tipo}
                                onChange={e=>setTipo(e.target.value)}
                                isInvalid={errores.tipo}
                            >
                                <Form.Label>Tipo</Form.Label>
                                <Form.Select defaultValue="Seleccione">
                                    <option value="">Seleccione</option>
                                    <option>Hábito</option>
                                    <option>Tarea</option>
                                </Form.Select>
                                <Form.Control.Feedback type="invalid">
                                    {errores.tipo}
                                </Form.Control.Feedback>
                            </Form.Group>
                            

                            <Form.Group 
                                
                                value={categoria}
                                onChange={e=>setCategoria(e.target.value)}
                                isInvalid={errores.categoria}
                            >
                                <Form.Label>Categoria</Form.Label>
                                <Form.Select defaultValue="Seleccione">
                                    <option value="">Seleccione</option>
                                    <option>Estudio</option>
                                    <option>Trabajo</option>
                                    <option>Deportes</option>
                                    <option>Hogar</option>
                                    <option>Mercado</option>
                                    <option>Aire libre</option>
                                    <option>Salud</option>
                                </Form.Select>
                                <Form.Control.Feedback type="invalid">
                                    {errores.categoria}
                                </Form.Control.Feedback>
                            </Form.Group>
                            

                            <Form.Group 
                                className="mt-3 mb-3"  
                                
                                value={detalle}
                                onChange={e=>setDetalle(e.target.value)}
                                isInvalid={errores.detalle}
                            >
                            <Form.Label>Detalle</Form.Label>
                            <Form.Control type="text"/>
                            <Form.Control.Feedback type="invalid">
                                    {errores.detalle}
                            </Form.Control.Feedback>
                            </Form.Group>
                            

                            <Form.Group 
                                
                                value={prioridad}
                                onChange={e=>setPrioridad(e.target.value)}
                                isInvalid={errores.prioridad}
                            >
                                <Form.Label>Prioridad</Form.Label>
                                <Form.Select defaultValue="Seleccione">
                                    <option value="">Seleccione</option>
                                    <option>Alta</option>
                                    <option>Normal</option>
                                    <option>Baja</option>
                                </Form.Select>
                                <Form.Control.Feedback type="invalid">
                                    {errores.prioridad}
                            </Form.Control.Feedback>
                            </Form.Group>
                            
                            <Button className="mt-3 mb-3" variant="primary" type="submit">
                                Crear Rutina
                            </Button>
                        </Form>
                    </div>
                </div>
            </div>  
        </div>
    )
}

export {CrearRutinaForm}
