import React, { useEffect, useState } from 'react'
import { Alert, Button, Form } from 'react-bootstrap'
import logo from '../img/Logo.png'
import AOS from 'aos';
import 'aos/dist/aos.css';
import { Link } from 'react-router-dom';

const SigninForm = ({errores, enviarCallback}) => {

    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")

    useEffect(() => {
        AOS.init({duration:2000});
    },[])

    const enviarFormulario=(e)=>{
        e.preventDefault();
        enviarCallback({userName,password})
    }

    return (
        <div className="container" data-aos="zoom-out">
            <div className="row">
                <div className="col-md-5 col-lg-4 mx-auto">
                    <div className="myform form shadow  mb-5 bg-body rounded-3 ">
                        <div className="col-md-12 d-flex justify-content-center">
                            <Link className="navbar-brand" to={"/"}>
                                <img src={logo} width="150" height="100" alt=""/>
                            </Link>
                        </div>

                        <Form onSubmit={enviarFormulario}>
                            
                            <Form.Group className="text-secondary mt-3 mb-3" controlId="correo">
                                <Form.Label>Usuario</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    value={userName}
                                    onChange={e=>setUserName(e.target.value)}
                                    isInvalid={errores.userName}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errores.userName}
                                </Form.Control.Feedback>
                                
                            </Form.Group>
                            
                            <Form.Group className=" text-secondary mb-3" controlId="password">
                                <Form.Label>Contraseña</Form.Label>
                                <Form.Control 
                                    type="password"
                                    value={password}
                                    onChange={e=>setPassword(e.target.value)}
                                    isInvalid={errores.password}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errores.password}
                                </Form.Control.Feedback>
                            </Form.Group>

                            {errores.autenticacion && <Alert variant="danger">{errores.autenticacion}</Alert>}
                            
                            <Button className="col-md-12 text-center mt-3" variant="primary" type="submit">
                            Iniciar Sesion
                            </Button>
                        </Form>
                        <div className=" fontsizeForm mt-3">
                            <p className="text-center text-secondary">¿No te has registrado? <Link to={'./usuarios'} id="signup">Registrate</Link></p>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        
    )
}

export {SigninForm}
