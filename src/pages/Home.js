import React, { useEffect } from 'react'
import logo from '../img/Logo.png'
import dentista from '../img/dentista.jpg'
import ejercicio from '../img/ejercicio.jpg'
import tarea from '../img/tarea.jpg'
import { Carousel,Nav } from 'react-bootstrap'
import { NavLink,Link } from 'react-router-dom'
import { BsFacebook, BsInstagram , BsTwitter } from "react-icons/bs";
import AOS from 'aos';
import 'aos/dist/aos.css';



const Home = () => {

    useEffect(() => {
        AOS.init({duration:2000});
    },[])

    return (
        <div>
            <header>
                <nav className="navbar navbar-expand-sm bg-warning navbar-Light">
                    <div className="container-fluid" >
                        <div className="collapse navbar-collapse justify-content-center " >
                            <a className="navbar-brand" href="/">
                                <img src={logo} width="100" height="50" className="d-inline-block align-top" alt=""/>
                            </a>
                            <ul className="navbar-nav ">
                                <Nav.Link className="nav-link link-secondary fw-bold" as={NavLink} to={'/'}>Caracteristicas</Nav.Link>
                                <Nav.Link className="nav-link link-secondary fw-bold" as={NavLink} to={'/'}>Contactanos</Nav.Link> 
                            </ul>
                        </div>
                        <div className="collapse navbar-collaps d-flex">
                            <ul className="navbar-nav link-secondary">
                                <Nav.Link className="nav-link link-secondary fw-bold" as={NavLink} to={'./login'}>Iniciar sesión</Nav.Link>
                                <Nav.Link className="nav-link btn btn-primary fw-bold colorRegistrate" as={NavLink} to={'./usuarios'}>Regístrate</Nav.Link>   
                            </ul>
                        </div>
                    </div>
                </nav>

            </header>
            <div className="container mt-5 pe-5">
                <div data-aos="zoom-out" className= "text-center">
                    <h1>Del agobio al control total</h1>
                    <br/>
                    <p>odoist te da la tranquilidad de saber que todo está debidamente organizado <br/>y registrado para que puedas progresar en las cosas que <br/>son realmente importantes para ti.</p>
                </div>
            </div>

            <div className="container mt-5" >
                <div className="row  align-items-center">
                    <div  data-aos="zoom-in-right" className="col p-4" >
                        <img  className="shadow  mb-5 bg-body rounded-3 " src="https://images.pexels.com/photos/811572/pexels-photo-811572.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" width="100%" alt=""/>
                    </div>
                    <div className="vr"></div>
                    
                    <div data-aos="zoom-in-left" className="col text-center">
                        
                        <h2>Titulo 2</h2>
                        <p>odoist te da la tranquilidad de saber que todo está debidamente organizado y registrado para que puedas progresar en las cosas que son realmente importantes para ti.</p>
                    
                    </div> 
                </div>
            </div>
            
            <div className="container mt-5">
                <div className="row align-items-center">
                    <div data-aos="zoom-in-right" className="col text-center">
                        <h2>Titulo 2</h2>
                        <p>odoist te da la tranquilidad de saber que todo está debidamente organizado y registrado para que puedas progresar en las cosas que son realmente importantes para ti.</p>
                    </div>

                    <div data-aos="zoom-in-left" className="col p-4" >
                        <img className="shadow  mb-5 bg-body rounded-3" src="https://images.pexels.com/photos/811572/pexels-photo-811572.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" width="100%" alt="" />
                    </div>
            
                </div>
            </div>
            
            <div className="container mt-5">
                <div className="row align-items-center">
                    <div data-aos="zoom-in-right" className="col p-4" >
                        <img className="shadow  mb-5 bg-body rounded-3" src="https://images.pexels.com/photos/811572/pexels-photo-811572.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" width="100%" alt="" />
                    </div>
                    <div className="vr"></div>
                    
                    <div data-aos="zoom-in-left" className="col text-center">
                        <h2>Titulo 2</h2>
                        <p>odoist te da la tranquilidad de saber que todo está debidamente organizado y registrado para que puedas progresar en las cosas que son realmente importantes para ti.</p>
                    </div> 
                </div>
            </div>

            <div className="container mt-5 pe-5">
                <div className= "text-center">
                    <h2>Titulo 2</h2>
                </div>
            </div>

            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    height="500"
                    src={dentista}
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={tarea}
                    height="500"
                    alt="Second slide"
                    />

                    <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={ejercicio}
                    height="500"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            
            <div className="bg-dark text-center text-white mt-5">
            <div className="container p-4">
            <section className="mb-4">
                
                <a className="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                ><BsFacebook/>
                </a>
        
                
                <a className="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                ><BsTwitter/>
                </a>
        
        
                
                <a className="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                ><BsInstagram/>
                </a>
        
            </section>
            
        
            
            <section className="">
                <form action="">
                
                <div className="row d-flex justify-content-center">
                    
                    <div className="col-auto">
                    <p className="pt-2">
                        <strong>Administra tus rutinas</strong>
                    </p>
                    </div>
                    
                    <div className="col-md-5 col-12">
                    
                    <div className="form-outline form-white mb-4">
                        <input type="email" id="form5Example2" className="form-control" placeholder="Correo Electrónico" />
                    </div>
                    </div>
                    
                    <div className="col-auto">
                    
                        <Link to={'./usuarios'}>
                            <button type="submit" className="btn btn-outline-light mb-4">Regístrate</button>
                        </Link>
                    
                    </div>
                    
                </div>
                
                </form>
            </section>
        </div>
        </div>
        </div>
    )
}

export {Home}
