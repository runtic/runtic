import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { EDITAR_RUTINAS_ENDPOINT } from '../helpers/endpoints'
import { CrearRutinaForm } from '../components/CrearRutinaForm'

const EditarRutina = () => {
    
    const {id}=useParams()
    const [rutina,setRutina]=useState([])
    const history=useHistory()
    

    useEffect(() => {
        axios.get(`${EDITAR_RUTINAS_ENDPOINT}/${id}`)
        .then(response =>{
            setRutina(response.data)
            
        })
        .catch(e =>{
            history.push('/')})
    },[id,history])

    return (
        <div>
            <CrearRutinaForm/>
            
        </div>
    )
}

export {EditarRutina};
