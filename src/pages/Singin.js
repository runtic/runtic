import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import {SigninForm} from '../components/SigninForm'
import validator from 'validator'
import { isObjetoVacio } from '../helpers/helpers'
import { useDispatch, useSelector} from 'react-redux'
import { loginUsuario } from '../helpers/auenticacionAcciones'


const Singin = () => {

    const [errores, setErrores] = useState({})
    const dispatch= useDispatch()
    const conectado=useSelector(state =>state.auth.conectado)
    const history = useHistory()

    useEffect(() => {
        if(conectado){
            history.push("/dasboard")
        }
    })

    const login=({userName,password})=>{

        const errores={}
        setErrores(errores)
        

        if(validator.isEmpty(userName)){
            errores.userName="Ingrese Usuario"
        }
        if(validator.isEmpty(password)){
            errores.password="Ingrese contraseña"
        }
        
        if(!isObjetoVacio(errores)){
            setErrores(errores);
            return;
        }
        
        dispatch(loginUsuario({userName,password}))
        .then(response =>{

        })
        .catch(error=>{
            setErrores({autenticacion:"Usuario o Contraseña invalida"})
        })
        
    }

    return (
        <div className="mt-5">
            <SigninForm errores={errores} enviarCallback={login}></SigninForm> 
        </div>
    )
}

export {Singin}
