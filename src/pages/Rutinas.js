import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { RUTINAS_CREADAS_ENDPOINT } from '../helpers/endpoints'

const Rutinas = () => {

    const [rutinas,setRutinas]=useState([])
    const [cargando,setCargando]=useState(true)

    useEffect(() => {
        axios.get(RUTINAS_CREADAS_ENDPOINT)
        .then(response =>{
            setRutinas(response.data)
            setCargando(false)
        })
        .catch(e =>{setCargando(false)})
    },[])


    return (
        <div>
            <div className="mt-5">
                <h5>  Mis Rutinas</h5>
                    <div>
                        <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>FECHA</th>
                                <th>TIPO</th>
                                <th>CATEGORIA</th>
                                <th>DETALLE</th>
                                <th>PRIORIDAD</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            {rutinas.map(
                                (rutina)=>

                                <tr key={rutina.id}>
                                <td>{rutina.fecha}</td>
                                <td>{rutina.tipo}</td>
                                <td>{rutina.categoria}</td>
                                <td>{rutina.detalle}</td>
                                <td>{rutina.prioridad}</td>
                                
                            
                                <td>
                                    <div className="btn-group" role="group" aria-label="">
                                        <Link className="btn btn-warning" 
                                        to={"/editar/"+rutina.id}
                                        
                                        >Editar</Link>
                                        
                                        
                                        <button type="button" className="btn btn-danger"
                                        onClick={()=>this.borrarRegistros(rutina.id)}
                                        >Borrar</button>

                                    </div>
                                </td>
                                
                            </tr>
                            )
                            }
                        </tbody>
                    </Table>
                </div>    
            </div>
        </div>
    )
}

export {Rutinas}
