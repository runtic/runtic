import React, { useState }  from 'react'
import { useHistory } from 'react-router-dom'
import validator from 'validator'
import { isObjetoVacio } from '../helpers/helpers'
import { useDispatch, useSelector} from 'react-redux'
import { crearRutina } from '../helpers/auenticacionAcciones'
import {CrearRutinaForm} from '../components/CrearRutinaForm'
import { Alert } from 'react-bootstrap'

const CrearRutina = () => {

    const [errores, setErrores] = useState({})
    const dispatch= useDispatch()
    const conectado=useSelector(state =>state.auth.conectado)
    const history = useHistory()

    const rutina=({fecha,tipo,categoria,detalle,prioridad})=>{

        
        const errores={}
        setErrores(errores)

        if(validator.isEmpty(fecha)){
            errores.fecha="Seleccione la fecha"
        }
        if(validator.isEmpty(tipo)){
            errores.tipo="Seleccione el tipo de actividad"
        }
        if(validator.isEmpty(categoria)){
            errores.categoria="Seleccione la categoria"
        }
        if(validator.isEmpty(detalle)){
            errores.detalle="Ingrese la actividad"
        }
        if(validator.isEmpty(prioridad)){
            errores.prioridad="Seleccione la prioridad"
        }
       
        
        if(!isObjetoVacio(errores)){
            setErrores(errores);
            return;
        }
        
        dispatch(crearRutina({fecha,tipo,categoria,detalle,prioridad}))
        .then(response =>{
            <Alert>Rutina Creada Exitosamente</Alert>
            window.location.href="/dashboard"

        })
        .catch(error=>{
        
        })
    }

    return (
        
        <div className="mt-5">
            <CrearRutinaForm errores={errores} enviarCallback={rutina}> </CrearRutinaForm>
        </div>
    )
}

export {CrearRutina}
