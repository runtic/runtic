import React from 'react'
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { useDispatch,useSelector } from 'react-redux'
import {  NavLink } from 'react-router-dom'
import { cerrarSesion } from '../helpers/auenticacionAcciones'
import logo from '../img/Logo.png'
import {Rutinas} from './Rutinas'


const Dashboard = () => {

    const conectado= useSelector(state => state.auth.conectado)
    const usuario = useSelector(state => state.auth.usuario)
    const dispatch=useDispatch()

    return (
        <React.Fragment>
            <Navbar bg="warning" expand="lg">
                <Container>
                    <Navbar.Brand as={NavLink} to={'/dashboard'}>
                        <img src={logo} width="150" height="50" className="d-inline-block align-top mx-5" alt=""/>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link as={NavLink} to={'/dashboard/crearRutina'}>Crear Rutina</Nav.Link>                     
                        </Nav>
                        <Nav>
                            <NavDropdown title={usuario.sub} id="basic-nav-dropdown">
                            <NavDropdown.Item onClick={() => dispatch(cerrarSesion())}>Cerrar sesión</NavDropdown.Item>
                            </NavDropdown>                        
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <div>
                <Rutinas/>
            </div>
        </React.Fragment>
    )
}

export {Dashboard}
