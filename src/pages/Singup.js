import React, { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import validator from 'validator'
import { isObjetoVacio } from '../helpers/helpers'
import { useDispatch, useSelector} from 'react-redux'
import { loginUsuario,registrarUsuario } from '../helpers/auenticacionAcciones'
import { SignupForm } from '../components/SignupForm'

const Singup = () => {

    const [errores, setErrores] = useState({})
    const dispatch= useDispatch()
    const conectado=useSelector(state =>state.auth.conectado)
    const history = useHistory()

    useEffect(() => {
        if(conectado){
            history.push("/")
        }
    })

    const registro=({userName,password, correo})=>{

        const errores={}
        setErrores(errores)
        

        if(validator.isEmpty(userName)){
            errores.userName="Ingrese su Usuario"
        }
        if(!validator.isLength(password,{min:8,max:30})){
            errores.password="La contraseña debe tener entre 8 y 30 caracteres"
        }
        if(!validator.isEmail(correo)){
            errores.correo="El correo electronico es invalido"
        }
        
        if(!isObjetoVacio(errores)){
            setErrores(errores);
            return;
        }
        
        dispatch(registrarUsuario({userName,password,correo}))
        .then(response =>{
            dispatch(loginUsuario({userName,password}))
        })
        .catch(error=>{
            setErrores({registro:error.response.date.menssage})
        })
        
    }

    return (
        <Container className="mt-5">
            
            <SignupForm errores={errores} enviarCallback={registro}></SignupForm>
    
        </Container>
    )
}

export {Singup}
